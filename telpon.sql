-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 19, 2019 at 09:21 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `telpon`
--

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `id_karyawan` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `noeks` varchar(10) NOT NULL,
  `dept` varchar(50) NOT NULL,
  `ip_komp` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`id_karyawan`, `nama`, `noeks`, `dept`, `ip_komp`) VALUES
(1, 'ASIS', '1652', 'Produksi CAP', '192.168.50.86'),
(2, 'JOKO MPC', '1314', 'Maintenance', '192.168.20.69'),
(3, 'Lilis / Technical', '1801', 'Technical', '192.168.20.125'),
(4, 'Jetty', '1702', 'Jetty', ''),
(5, 'Agus Jambrong', '1901', 'safety', ''),
(6, 'VCM Office', '2202', 'VCM', ''),
(7, 'Maman', '1504', 'IT', ''),
(8, 'Irsal', '1504', 'IT', ''),
(9, 'Donny', '(7)8702', 'IT Jakarta', ''),
(10, 'Irwan', '(7)8705', 'Purchasing', ''),
(11, 'Parno ', '1502', 'Warehouse', '192.168.20.35'),
(12, 'Abhinav Gupta', '5100', 'General Manager', ''),
(13, 'Hari SW', '1306/1305', 'Maintenance', ''),
(14, 'Rival', '1307', 'Maintenance', ''),
(15, 'Wanto', '1804', 'Technical', ''),
(16, 'Budi Aris. H', '5000', 'Maintenance', ''),
(17, 'Mach Ant Herson', '5003', 'Plant Manager', ''),
(18, 'Spv PVC', '3202', 'PVC CCR', '192.168.20.200'),
(19, 'Rohmat', '3201', 'Maintenance PVC', '192.168.50.68'),
(20, 'Levi', '1651', 'Maintenance', 'LEVI-NB');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `karyawan`
--
ALTER TABLE `karyawan`
  MODIFY `id_karyawan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
