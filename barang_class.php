<?php

 class karyawan

 {

      private $db;

      public function __construct($con){

           $this->db = $con;

      }

      ### Start : fungsi insert data ke database ###

      public function insertData($id_karyawan,$nama,$noeks,$ip_komp,$dept){

           try{

                $stmt = $this->db->prepare("INSERT INTO karyawan(id_karyawan,nama,noeks,ip_komp,dept) VALUES(:id_karyawan, :nama, :noeks, :ip_komp, :dept)");

                $stmt->bindparam(":id_karyawan",$id_karyawan);

                $stmt->bindparam(":nama",$nama);

                $stmt->bindparam(":noeks",$noeks);
				
				$stmt->bindparam(":ip_komp",$ip_komp);

                $stmt->bindparam(":dept",$dept);

                $stmt->execute();

                return true;

           }catch(PDOException $e){

                echo $e->getMessage();     

                return false;

           }

      }

      ### End : fungsi insert data ke database ###

      ### Start : fungsi ambil data dari database ###

      public function getID($id_karyawan){

           $stmt = $this->db->prepare("SELECT * FROM karyawan WHERE id_karyawan=:id_karyawan");

           $stmt->execute(array(":id_karyawan"=>$id_karyawan));

           $data=$stmt->fetch(PDO::FETCH_ASSOC);

           return $data;

      }

      ### End: fungsi ambil data dari database ###

      ### Start : fungsi untuk menampilkan data dari database ###

      public function viewData($query){

           $stmt = $this->db->prepare($query);

           $stmt->execute();

           if($stmt->rowCount()>0){

                while($row=$stmt->fetch(PDO::FETCH_ASSOC)){

                     ?>

         <tr>

         <td><?php echo($row['id_karyawan']); ?></td>

         <td><?php echo($row['nama']); ?></td>

         <td><?php echo($row['noeks']); ?></td>
		 
		 <td><?php echo($row['ip_komp']); ?></td>

         <td><?php echo($row['dept']); ?></td>

         <td align="center">

         <a href="edit.php?edit_id=<?php echo($row['id_karyawan']); ?>">

                     <i class="glyphicon glyphicon-edit"></i></a>

         </td>

         <td align="center">

         <a href="hapus.php?delete_id=<?php echo($row['id_karyawan']); ?>">

                     <i class="glyphicon glyphicon-remove-circle"></i></a>

         </td>

         </tr>

         <?php

                }

           }else{

                ?>

            <tr>

            <td>Data tidak ditemukan...</td>

            </tr>

       <?php

           }

      }

      ### End : fungsi untuk menampilkan data dari database ###

      ### Start : fungsi untuk memperbaharui data###

      public function updateData($id_karyawan,$nama,$noeks,$ip_komp,$dept){

           try{

                $stmt=$this->db->prepare("UPDATE karyawan SET nama=:nama,

                                                                       noeks=:noeks,
																	   
																	   ip_komp=:ip_komp,

                                                                       dept=:dept

                                                                  WHERE id_karyawan=:id_karyawan ");

                $stmt->bindparam(":id_karyawan",$id_karyawan);

                $stmt->bindparam(":nama",$nama);

                $stmt->bindparam(":noeks",$noeks);
				
				$stmt->bindparam(":ip_komp",$ip_komp);

                $stmt->bindparam(":dept",$dept);

                $stmt->execute();

                return true;     

           }catch(PDOException $e){

                echo $e->getMessage();     

                return false;

           }

      }

      ### End : fungsi untuk memperbaharui data###

      ### Start : fungsi untuk menghapus data###

      public function deleteData($id_karyawan){

           $stmt = $this->db->prepare("DELETE FROM karyawan WHERE id_karyawan=:id_karyawan");

           $stmt->bindparam(":id_karyawan",$id_karyawan);

           $stmt->execute();

           return true;

      }

      ### End : fungsi untuk menghapus data###

      ### Start : fungsi paging###

      public function paging($query,$records_per_page){

           $starting_position=0;

           if(isset($_GET["page_no"])){

                $starting_position=($_GET["page_no"]-1)*$records_per_page;

           }

           $query2=$query." limit $starting_position,$records_per_page";

           return $query2;

      }

      ### End : fungsi paging###

      ### Start : fungsi pindah page###

      public function paginglink($query,$records_per_page){

           $self = $_SERVER['PHP_SELF'];

           $stmt = $this->db->prepare($query);

           $stmt->execute();

           $total_no_of_records = $stmt->rowCount();

           if($total_no_of_records > 0){

                ?><ul class="pagination"><?php

                $total_no_of_pages=ceil($total_no_of_records/$records_per_page);

                $current_page=1;

                if(isset($_GET["page_no"])){

                     $current_page=$_GET["page_no"];

                }

                if(isset($_GET['nama'])){
                     $cari="&nama=".$_GET['nama']."&ext=".$_GET['ext']."&ip=".$_GET['ip']."&dep=".$_GET['dep'];
                }else{
                     $cari='';
                }

                if($current_page!=1){

                     $previous =$current_page-1;

                     echo "<li><a href='".$self."?page_no=1'".$cari.">First</a></li>";

                     echo "<li><a href='".$self."?page_no=".$previous,$cari."'>Previous</a></li>";

                }

                for($i=1;$i<=$total_no_of_pages;$i++){

                     if($i==$current_page){

                          echo "<li><a href='".$self."?page_no=".$i,$cari."' style='color:red;'>".$i."</a></li>";

                     }else{

                          echo "<li><a href='".$self."?page_no=".$i,$cari."'>".$i."</a></li>";

                     }

                }

                if($current_page!=$total_no_of_pages){

                     $next=$current_page+1;

                     echo "<li><a href='".$self."?page_no=".$next,$cari."'>Next</a></li>";

                     echo "<li><a href='".$self."?page_no=".$total_no_of_pages,$cari."'>Last</a></li>";

                }

                ?></ul><?php

           }

      }

      ### End : fungsi pindah page###

 }
