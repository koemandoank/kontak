 <?php

      include_once 'dbconfig.php';

      $id_karyawan      = $_GET['edit_id'];

      if(isset($_POST['btn-update'])){

           $id_karyawan           = $_POST['id_karyawan'];
		   
		   $nama           = $_POST['nama'];

           $noeks          = $_POST['noeks'];
		   
		   $ip_komp          = $_POST['ip_komp'];

           $dept          = $_POST['dept'];

           if($brg->updateData($id_karyawan,$nama,$noeks,$ip_komp,$dept)){

                $msg = "<div class='alert alert-info'>

                          <strong>Info</strong> Data berhasil diubah! Silakan klik di <a href='index.php'>sini</a> untuk kembali ke beranda.

                          </div>";

           }else{

                $msg = "<div class='alert alert-warning'>

                          <strong>Warning!</strong> Update Data Gagal !

                          </div>";

           }

      }

      if(isset($id_karyawan)){

           extract($brg->getID($id_karyawan));     

      }

 ?>

 <!DOCTYPE html>

 <html lang="en">

 <head>

   <title>Edit Muka Lu...!!!</title>

   <meta charset="utf-8">

   <meta name="viewport" content="width=device-width, initial-scale=1">

   <!--Bootstrap-->

   <link href="http://<?php echo $_SERVER['SERVER_NAME'];?>/kontak/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

 </head>

 <body>

   <div class="container">

     <div class="panel panel-primary">

      <div class="panel-heading">Form Edit Data</div>

      <div class="panel-body">

                <?php

                     if(isset($msg))

                     {

                          echo $msg;

                     }

                ?>

                </div>

                <div class="clearfix"></div><br />

                <form method='post'>

                  <table class='table table-bordered'>

                    <tr>

                      <td>No Urut</td>

                      <td><input type='text' name='id_karyawan' class='form-control' required maxlength="10" value="<?php echo $id_karyawan; ?>" readonly></td>

                    </tr>

                    <tr>

                      <td>Nama Karyawan</td>

                      <td><input type='text' name='nama' class='form-control' required maxlength="50" value="<?php echo $nama; ?>" autofocus></td>

                    </tr>

                    <tr>

                      <td>No Ekstension</td>

                      <td><input type='text' name='noeks' class='form-control' value="<?php echo $noeks; ?>" required></td>

                    </tr>
					
					<tr>

                      <td>IP Komputer</td>

                      <td><input type='text' name='ip_komp' class='form-control' value="<?php echo $ip_komp; ?>" required></td>

                    </tr>

                    <tr>

                      <td>Department</td>

                      <td><input type='text' name='dept' class='form-control' value="<?php echo $dept; ?>" required></td>

                    </tr>

                    <tr>

                      <td colspan="2">

                      <button type="submit" class="btn btn-primary" name="btn-update">Simpan

                               </button> 

                               <button type="reset" class="btn btn-primary" name="btn-reset">Reset

                               </button> <br/><br/>  

                      <a href="index.php" class="btn btn-large btn-success">

                               <i class="glyphicon glyphicon-backward"></i> &nbsp; Kembali ke halaman utama</a>

                      </td>

                    </tr>

                  </table>

                </form>

      </div><!--End: Panel-body-->

     </div><!--End: Panel-->

   </div>

   <div class="container">

     <div class="alert alert-success">

       <p><strong>Selamat Bekerja :) </strong></p>

       <p>If you have question, feel free to ask me <a href="http://facebook.com/unknown.trozan">here</a>!</p>

     </div>

   </div>

   <!--Bootstrap-->

   <script src="http://<?php echo $_SERVER['SERVER_NAME'];?>/kontak/bootstrap-3.3.7-dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

 </body>

 </html>