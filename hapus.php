<?php

   include_once 'dbconfig.php';

   if(isset($_POST['btn-del'])){

     $id_karyawan = $_GET['delete_id'];

        $brg->deleteData($id_karyawan);

        header("Location: hapus.php?deleted");     

   }

 ?>

 <!DOCTYPE html>

 <html lang="en">

 <head>

   <title>Form Hapus Kenangan Mantan</title>

   <meta charset="utf-8">

   <meta name="viewport" content="width=device-width, initial-scale=1">

   <!--Bootstrap-->

   <link href="http://<?php echo $_SERVER['SERVER_NAME'];?>/kontak/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

 </head>

 <body>

   <div class="container">

     <div class="panel panel-primary">

      <div class="panel-heading">Halaman Hapus Data</div>

      <div class="panel-body">

   <?php

   if(isset($_GET['deleted']))

   {

     ?>

     <div class="alert alert-success">

     <strong>Info!</strong> Data berhasil dihapus... 

     </div>

     <?php

   }

   else

   {

     ?>

     <div class="alert alert-danger">

     <strong>Warning!</strong> apa anda yakin ingin menghapusnya ? 

     </div>

     <?php

   }

   ?> 

 </div>

 <div class="container">

   <?php

     if(isset($_GET['delete_id'])){

       $id_karyawan = $_GET['delete_id'];

       ?>

         <table class='table table-bordered'>

         <tr>

           <th>#</th>

           <th>Nama</th>

           <th>No Ekstension</th>
		   
		   <th>IP Komputer</th>

           <th>Department</th>

         </tr>

       <?php

         extract($brg->getID($id_karyawan));

       ?>

         <tr>

         <td><?php echo $id_karyawan; ?></td>

         <td><?php echo $nama; ?></td>

         <td><?php echo $noeks; ?></td>
		 
		 <td><?php echo $ip_komp; ?></td>

         <td><?php echo $dept; ?></td>

         </tr>

         </table>

       <?php

     }

   ?>

 </div>

 <div class="container">

 <p>

 <?php

 if(isset($id_karyawan))

 {

   ?>

   <form method="post">

   <input type="hidden" name="id_karyawan" value="<?php echo $id_karyawan; ?>" />

   <button class="btn btn-large btn-primary" type="submit" name="btn-del">

   <i class="glyphicon glyphicon-trash"></i> &nbsp; YES</button>

   <a href="index.php" class="btn btn-large btn-success">

   <i class="glyphicon glyphicon-backward"></i> &nbsp; NO</a>

   </form> 

   <?php

 }

 else

 {

   ?>

   <a href="index.php" class="btn btn-large btn-success">

   <i class="glyphicon glyphicon-backward"></i> &nbsp; Back to index</a>

   <?php

 }

 ?>

 </p>

 </div> 

      </div><!--End: Panel-body-->

     </div><!--End: Panel-->

   </div>

   <div class="container">

     <div class="alert alert-success">

       <p><strong>Selamat Bekerja :) </strong></p>

       <p>If you have question, feel free to ask me <a href="http://facebook.com/unknown.trozan">here</a>!</p>

     </div>

   </div>

   <!--Bootstrap-->

   <script src="http://<?php echo $_SERVER['SERVER_NAME'];?>/kontak/bootstrap-3.3.7-dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

 </body>

 </html>
